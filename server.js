'use strict';
var express = require('express'),
  app = express(),
  port = process.env.PORT || 80,
  bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

var routes = require('./routes');
routes(app);

app.listen(port);

console.log('server started on: ' + port);
