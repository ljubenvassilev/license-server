'use strict';

const logger = require('./logger').logger;

exports.check = function(req, res){
  var license = req.body.license;
  console.log('license number ' + license);
  switch(license){
    case 'test':
    case 'SalaMariiUniri':
    case 'Mjolnir Project':
//    case 'Licence Test':
      res.sendStatus(200);
      break;
    default:
      res.sendStatus(400);
  }
};

exports.android = function(req, res){
  var license = req.body.license;
  console.log('android license ' + license);
  switch(license) {
    case 'd4d720566735792d0bc0e490d68e1d15':
      res.sendStatus(200);
      logger.info('Authorized access by: ' + license);
      break;
    default:
      logger.info('Unauthorized access by: ' + license);
      res.sendStatus(400);
  }
};
