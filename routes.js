'use strict';
module.exports = function(app) {
  var controller = require('./controller');
  app.route('/check').post(controller.check);
  app.route('/android').post(controller.android);
};
